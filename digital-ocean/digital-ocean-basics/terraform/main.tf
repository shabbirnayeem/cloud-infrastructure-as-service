data "digitalocean_ssh_key" "lab_key" {
  name = "lab_key"
}

resource "digitalocean_droplet" "app-server" {
  image  = "ubuntu-22-04-x64"
  name   = "app-server"
  region = "nyc1"
  size   = "s-1vcpu-1gb"
  ssh_keys = [
    data.digitalocean_ssh_key.lab_key.id,
  ]

  connection {
    host        = self.ipv4_address
    user        = "root"
    type        = "ssh"
    private_key = file(var.pvt_key)
    timeout     = "2m"
  }
  provisioner "remote-exec" {
    inline = [
      "export NEEDRESTART_MODE=a",
      "export DEBIAN_FRONTEND=noninteractive",
      "apt update",
      "apt install default-jre -y",
      "apt install default-jdk -y",
    ]
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u root -i '${self.ipv4_address},' --private-key ${var.pvt_key} configure-app-server.yaml"
  }
}

resource "digitalocean_firewall" "app-server-fw" {
  name        = "app-server-fw"
  droplet_ids = [digitalocean_droplet.app-server.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = [var.my_ip]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "7071"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

}

output "app-server-ip" {
  value = digitalocean_droplet.app-server.ipv4_address
}
