# Module 5 - Cloud & Infrastructure as Service Basics

## Demo Project:
Create server and deploy application on DigitalOcean

## Technologiesused:
DigitalOcean, Linux, Java, Gradle, Terraform

## Project Description:
- Setup and configure a server on DigitalOcean using Terraform
- Create and configure a new Linux user on the Droplet (Security best practice)
- Deploy and run a Java Gradle application on Droplet
